package main

import (
  "fmt"
  "net/http"

  "github.com/labstack/echo"
  "github.com/labstack/echo/middleware"
)

func main() {
  e := echo.New()
  e.Use(middleware.Recover())

  e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
    Format: "${time_rfc3339_nano}  ${method}  ${uri}  ${status}  ${error}\n",
  }))

  e.GET("/", func(c echo.Context) error {
    return c.String(http.StatusOK, "Echo base route")
  })

  e.GET("/other", func(c echo.Context) error {
    res := map[string]interface{}{
      "response": "Echo other route",
    }
    return c.JSON(http.StatusOK, res)
  })

  port := 9000
  portStr := fmt.Sprintf(":%d", port)
  msg := fmt.Sprintf("Starting Echo server on port %d", port)
  fmt.Println(msg)

  e.Logger.Fatal(e.Start(portStr))
}
