require 'sinatra'
require 'json'

get '/' do
  'Sinatra base route'
end

get '/other' do
  { 'response': 'Sinatra other route' }.to_json
end
