# README

Performance comparison of some popular web server frameworks:

- Node.js _Express_
- Node.js _Koa_
- Node.js _Hapi_
- Python _FastAPI_ (with _Uvicorn_)
- Python _Flask_ (with _Gunicorn_)
- Ruby _Sinatra_ (with _Puma_)
- Go _Echo_
- Go _Fiber_

Tests carried out with the HTTP benchmark tool [wrk](https://github.com/wg/wrk), using this [Docker image](https://hub.docker.com/r/williamyeh/wrk).

This setup is for testing minimal web servers on `localhost`. It could be useful to test on a live server, of course.

Here is a useful reference [article](https://www.digitalocean.com/community/tutorials/how-to-benchmark-http-latency-with-wrk-on-ubuntu-14-04).

## App Information

App Name: servers-test

Created: May 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/servers-test)

## To Run

```
$ docker-compose up -d --build
$ bash run_test.sh
```

## Tests

This is an example command to test each server:

```
wrk -t2 -c5 -d5s --timeout 2s http://host.docker.internal:[PORT]/
```

- Use two separate threads (-t2)
- Open six connections (the first is zero) (-c5)
- Run test for five seconds (-d5s)
- Add a two-second timeout (--timeout 2s)

_Note_: `host.docker.internal` is used to get the local Docker IP.

### Test Results

Best of 3-4 tries. Ordered from fastest to slowest.

1. Echo

```
Running 5s test @ http://host.docker.internal:9000/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.29ms    1.96ms  42.62ms   99.30%
    Req/Sec     1.72k    97.06     2.13k    90.00%
  17143 requests in 5.00s, 2.16MB read
Requests/sec:   3425.53
Transfer/sec:    441.57KB
```

2. Fiber

```
Running 5s test @ http://host.docker.internal:9500/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.18ms  194.96us   5.00ms   76.57%
    Req/Sec     1.70k    58.58     2.01k    83.00%
  16974 requests in 5.00s, 2.15MB read
Requests/sec:   3393.42
Transfer/sec:    440.75KB
```

3. Koa

```
Running 5s test @ http://host.docker.internal:5100/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.20ms  222.12us   5.96ms   77.37%
    Req/Sec     1.68k    82.19     2.09k    88.00%
  16738 requests in 5.00s, 2.84MB read
Requests/sec:   3346.84
Transfer/sec:    581.77KB
```

4. Flask

```
Running 5s test @ http://host.docker.internal:8500/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.48ms  511.11us  16.28ms   95.88%
    Req/Sec     1.37k    58.73     1.48k    77.00%
  13644 requests in 5.00s, 2.26MB read
Requests/sec:   2727.33
Transfer/sec:    463.43KB
```

5. Sinatra

```
Running 5s test @ http://host.docker.internal:4567/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.54ms  358.91us   3.84ms   75.38%
    Req/Sec     1.30k    41.92     1.37k    69.00%
  12926 requests in 5.00s, 2.35MB read
Requests/sec:   2582.87
Transfer/sec:    481.77KB
```

6. Express

```
Running 5s test @ http://host.docker.internal:5000/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.63ms  551.36us  10.19ms   91.71%
    Req/Sec     1.25k   105.14     1.42k    58.00%
  12426 requests in 5.00s, 2.92MB read
Requests/sec:   2483.30
Transfer/sec:    596.57KB
```

7. Hapi

```
Running 5s test @ http://host.docker.internal:5200/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.64ms  453.06us   8.40ms   77.64%
    Req/Sec     1.23k   138.15     2.00k    80.20%
  12330 requests in 5.10s, 2.65MB read
Requests/sec:   2417.73
Transfer/sec:    531.24KB
```

8. FastAPI

```
Running 5s test @ http://host.docker.internal:8000/
  2 threads and 5 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.87ms  327.46us   6.63ms   72.52%
    Req/Sec     1.07k    77.17     1.23k    59.00%
  10660 requests in 5.01s, 1.55MB read
Requests/sec:   2129.80
Transfer/sec:    316.14KB
```

Last updated: 2025-02-24
