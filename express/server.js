const express = require("express");
const morgan = require("morgan");

const app = express();
const PORT = 5000;

app.use(morgan("common"));

app.get("/", (req, res) => {
  res.send("Express base route");
});

app.get("/other", (req, res) => {
  res.json({ response: "Express other route" });
});

app.listen(PORT, () => {
  console.log(`Express server running on port ${PORT}`);
});
