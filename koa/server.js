const Koa = require("koa");
const Router = require("@koa/router");
const morgan = require("koa-morgan");

const app = new Koa();
const router = new Router();
const PORT = 5100;

app.use(morgan("common"));

router.get("base", "/", (ctx) => {
  ctx.body = "Koa base route";
});

router.get("other", "/other", (ctx) => {
  ctx.body = { response: "Koa other route" };
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(PORT, () => {
  console.log(`Koa server running on port ${PORT}`);
});
