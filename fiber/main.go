package main

import (
  "fmt"

  "github.com/gofiber/fiber"
  "github.com/gofiber/fiber/middleware/logger"
)

func main() {
  app := fiber.New()
  app.Use(logger.New())

  app.Get("/", func(c *fiber.Ctx) error {
    return c.SendString("Fiber base route")
  })

  app.Get("/other", func(c *fiber.Ctx) error {
    res := map[string]interface{}{
      "response": "Fiber other route",
    }
    return c.JSON(res)
  })

  port := 9500
  portStr := fmt.Sprintf(":%d", port)
  msg := fmt.Sprintf("Starting Fiber server on port %d", port)
  fmt.Println(msg)

  app.Listen(portStr)
}
