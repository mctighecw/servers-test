#!/bin/bash

# Routes to test:
# base route: ""
# other route: other

read -p "Enter port: " app_port
read -p "Enter route: " app_route

echo Testing app with port $app_port...

docker run --rm williamyeh/wrk -t2 -c5 -d5s --timeout 2s http://host.docker.internal:$app_port/$app_route
