from fastapi import FastAPI, Response
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

app = FastAPI()

@app.get("/")
def base_root():
    return Response(content="FastAPI base route", media_type="text/plain")

@app.get("/other")
def other_root():
    content = jsonable_encoder({ "response": "FastAPI other route" })
    return JSONResponse(content=content)
