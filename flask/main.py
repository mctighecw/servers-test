from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/")
def base_route():
    return "Flask base route"

@app.route("/other")
def test_route():
    res = jsonify({ "response": "Flask other route"})
    return res
