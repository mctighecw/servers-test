const Hapi = require("@hapi/hapi");
const laabr = require("laabr");

const init = async () => {
  const server = Hapi.server({
    port: 5200,
    host: "0.0.0.0",
  });

  await server.register({
    plugin: laabr,
    options: {},
  });

  server.route({
    method: "GET",
    path: "/",
    handler: (request, h) => {
      return "Hapi base route";
    },
  });

  server.route({
    method: "GET",
    path: "/other",
    handler: (request, h) => {
      return h.response({ response: "Hapi other route" });
    },
  });

  await server.start();
  console.log("Hapi server running on port %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
